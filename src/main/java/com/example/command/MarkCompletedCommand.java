package com.example.command;

import org.axonframework.commandhandling.annotation.TargetAggregateIdentifier;

/**
 * Created by pado on 10/9/16.
 */
public class MarkCompletedCommand {
    @TargetAggregateIdentifier
    private final String todoId;

    public MarkCompletedCommand(String todoId) {
        this.todoId = todoId;
    }

    public String getTodoId() {
        return todoId;
    }
}
