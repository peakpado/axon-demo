package com.example.command;

/**
 * Created by pado on 10/9/16.
 */
public class ToDoItemCompletedEvent {
    private final String todoId;

    public ToDoItemCompletedEvent(String todoId) {
        this.todoId = todoId;
    }

    public String getTodoId() {
        return todoId;
    }
}
