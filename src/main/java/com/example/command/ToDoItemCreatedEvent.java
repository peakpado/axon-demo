package com.example.command;

/**
 * Created by pado on 10/9/16.
 */
public class ToDoItemCreatedEvent {
    private String todoId = null;
    private String description = null;

    public ToDoItemCreatedEvent() {
    }

    public ToDoItemCreatedEvent(String todoId, String description) {
        this.todoId = todoId;
        this.description = description;
    }

    public String getTodoId() {
        return todoId;
    }

    public String getDescription() {
        return description;
    }
}
