package com.example.command;

import org.axonframework.eventhandling.annotation.EventHandler;

/**
 * Created by pado on 10/9/16.
 */
//@Component
public class ToDoEventHandler {
    @EventHandler
    public void handle(ToDoItemCreatedEvent event) {
        System.out.println("We've got something to do: " + event.getDescription() + " (" + event.getTodoId() + ")");
    }

    @EventHandler
    public void handle(ToDoItemCompletedEvent event) {
        System.out.println("We've completed a task: " + event.getTodoId());
    }
}
