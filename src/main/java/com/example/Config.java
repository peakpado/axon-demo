package com.example;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by pado on 10/9/16.
 */
@Configuration
@ImportResource("classpath:config.xml")
@EntityScan(basePackages = {"com.example.aggregate", "org.axonframework.eventstore.jpa"})
public class Config {
}
