package com.example.repository;

import com.example.MyController;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.axonframework.serializer.json.JacksonSerializer;

/**
 * Created by pado on 10/15/16.
 */
public class MyJacksonSerializer extends JacksonSerializer {
    private static final Logger LOG = LogManager.getLogger(MyController.class);

    public MyJacksonSerializer() {
        super();

        // configure object mapper
        ObjectMapper mapper = getObjectMapper();
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }
}
