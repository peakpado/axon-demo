package com.example.repository;

import org.axonframework.common.jpa.EntityManagerProvider;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by pado on 10/13/16.
 */
@Component
public class MyEntityManagerProvider implements EntityManagerProvider {

    private EntityManager entityManager;

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    @PersistenceContext(unitName = "default")
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
