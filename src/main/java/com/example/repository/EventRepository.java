package com.example.repository;

import org.axonframework.eventstore.jpa.DomainEventEntry;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by pado on 10/13/16.
 */
public interface EventRepository extends CrudRepository<DomainEventEntry, String> {
    List<DomainEventEntry> findAllByAggregateIdentifier(String aggregateIdentifier);
}
