package com.example.repository;

/**
 * Created by pado on 10/13/16.
 */
public class EventInfo {
    private String aggregateIdentifier;
    private Long sequenceNumber;
    private String eventIdentifier;
    private String type;

    public String getAggregateIdentifier() {
        return aggregateIdentifier;
    }

    public void setAggregateIdentifier(String aggregateIdentifier) {
        this.aggregateIdentifier = aggregateIdentifier;
    }

    public Long getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getEventIdentifier() {
        return eventIdentifier;
    }

    public void setEventIdentifier(String eventIdentifier) {
        this.eventIdentifier = eventIdentifier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
