package com.example.aggregate;

import com.example.command.CreateToDoItemCommand;
import com.example.command.MarkCompletedCommand;
import com.example.command.ToDoItemCompletedEvent;
import com.example.command.ToDoItemCreatedEvent;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.axonframework.commandhandling.annotation.CommandHandler;
import org.axonframework.eventhandling.annotation.EventHandler;
import org.axonframework.eventsourcing.annotation.AbstractAnnotatedAggregateRoot;
import org.axonframework.eventsourcing.annotation.AggregateIdentifier;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by pado on 10/9/16.
 */
@Entity
@JsonIgnoreProperties
public class ToDoItem extends AbstractAnnotatedAggregateRoot {

    @Id
    @AggregateIdentifier
    private String identifier;

    public ToDoItem() {
    }

    @CommandHandler
    public ToDoItem(CreateToDoItemCommand command) {
        apply(new ToDoItemCreatedEvent(command.getTodoId(), command.getDescription()));
    }

    @CommandHandler
    public void markComplated(MarkCompletedCommand command) {
        apply(new ToDoItemCompletedEvent(identifier));
    }

    @EventHandler
    public void on(ToDoItemCreatedEvent event) {
        this.identifier = event.getTodoId();
    }
}
