package com.example;

import com.example.command.CreateToDoItemCommand;
import com.example.command.MarkCompletedCommand;
import com.example.repository.EventInfo;
import com.example.repository.EventRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventstore.jpa.DomainEventEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by pado on 10/9/16.
 */
@RestController
public class MyController {
    private static final Logger LOG = LogManager.getLogger(MyController.class);

    @Autowired
    private CommandGateway commandGateway;
    @Autowired
    private EventRepository eventRepository;

    @Transactional
    @RequestMapping(value="/todo", method = RequestMethod.GET)
    public String todo() {

        String itemId = UUID.randomUUID().toString();
        commandGateway.send(new CreateToDoItemCommand(itemId, "Let do this"));

        return "{\"message\": \"" + itemId + "\"}";
    }

    @Transactional
    @RequestMapping(value="/mark", method = RequestMethod.GET)
    public String mark(@RequestParam("aggregateId") String aggregateId) {
        commandGateway.send(new MarkCompletedCommand(aggregateId));
        return "{\"message\": \"Marked\"}";
    }

    @RequestMapping("/events")
    public List<EventInfo> events() {
        List<EventInfo> infos = new ArrayList<>();
        for (DomainEventEntry e : eventRepository.findAll()) {
//            <com.example.command.ToDoItemCreatedEvent><todoId>d388c35a-0401-4f8c-b7ad-3040d87354e3</todoId><description>Let do this</description></com.example.command.ToDoItemCreatedEvent>
            LOG.info(e.getPayload().toString() + ": " + new String(e.getPayload().getData()));
//            org.axonframework.serializer.SerializedMetaData@6ee2dc70: <meta-data/>
            LOG.info(e.getMetaData().toString() + ": " + new String(e.getMetaData().getData()));
            EventInfo info = new EventInfo();
            info.setAggregateIdentifier(e.getAggregateIdentifier().toString());
            info.setEventIdentifier(e.getEventIdentifier());
            info.setSequenceNumber(e.getSequenceNumber());
            info.setType(e.getType());
            infos.add(info);
        }
        return infos;
    }
}
